#include <SFML/Graphics.hpp>
#define LARGEUR_FENETRE 1280
#define HAUTEUR_FENETRE 720
#define TAILLE_BATIMENT 305
#define TAILLE_ELEPHANT 150
using namespace sf;

void DeplacementElephant(int PAS, int positionX);
void ToucheElephant();


int main()
{
    int PAS = 10;
    int positionX = 100;
    DeplacementElephant(PAS, positionX);
    //void ToucheElephant();
}

void DeplacementElephant(int PAS, int positionX)
{
    int P = 10;
    int posiX = 100;

    Texture fond;
    fond.loadFromFile("Fond.png");
    Sprite FOND;
    FOND.setTexture(fond);

    RenderWindow fenetre(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Elephantastique");
    Texture image;
    image.loadFromFile("elephant.png");
    Sprite ELEPHANT;
    ELEPHANT.setTexture(image);
    ELEPHANT.setPosition(posiX, 480);
    FloatRect dim= ELEPHANT.getGlobalBounds();
    printf("dimension de l'elephant %f\n", dim.width);

    Event evenement;
    while (fenetre.isOpen())
    {
        fenetre.clear(Color::Black);
        fenetre.draw(FOND);
        fenetre.draw(ELEPHANT);
        fenetre.display();
        while (fenetre.pollEvent(evenement))
        {
            switch (evenement.type)
            {
            case Event::KeyPressed:
                if (evenement.key.code == Keyboard::Left & posiX > 75)
                {
                    ELEPHANT.setOrigin(TAILLE_ELEPHANT/2, 0);
                    ELEPHANT.setScale(1.0f, 1.0f);
                    ELEPHANT.setPosition(posiX -= P, 480);
                    posiX = posiX - P;

                }
                if (evenement.key.code == Keyboard::Right & posiX < LARGEUR_FENETRE - TAILLE_BATIMENT - TAILLE_ELEPHANT/2)
                {
                    ELEPHANT.setOrigin(TAILLE_ELEPHANT/2, 0);
                    ELEPHANT.setScale(-1.0f, 1.0f);
                    ELEPHANT.setPosition(posiX += P, 480);
                    posiX = posiX + P;

                }
            }
        }
    }

}

//void ToucheElephant()
//{
 //   FloatRect elephantBox = ELEPHANT.getGlobalBounds();
   // FloatRect objetBox = OBJET.getGlobalBounds();
  //  if (objetBox.intersects(elephantBox));
  //      printf("L'objet touche l'elephant"); //A modifier pour que le game over se lance, ou l'elephant perde de la vie
//}

