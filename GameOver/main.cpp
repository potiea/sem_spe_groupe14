#include <SFML/Graphics.hpp>
#define LARGEUR_FENETRE 1280
#define HAUTEUR_FENETRE 720
using namespace sf;

int main()
{
    RenderWindow fenetre(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "GameOver");

    Font font;
    font.loadFromFile("JAi_____.TTF");

    Font font1;
    font1.loadFromFile("AGENTORANGE.TTF");

    Text text;
    text.setFont(font);
    text.setString("Press Enter to continue");
    text.setPosition(Vector2f(442, 500));
    FloatRect dimension2 = text.getGlobalBounds();
    printf("dimension du texte enter %f\n", dimension2.width);

    Text gameover;
    gameover.setFont(font1);
    gameover.setString("Game \n Over");
    gameover.setPosition(Vector2f(429,200));
    gameover.setCharacterSize(100);
    FloatRect dimension1 = gameover.getGlobalBounds();
    printf("dimension du texte game over %f\n", dimension1.width);

    Text text1;
    text1.setFont(font);
    text1.setString("Press Escape to continue");
    text1.setPosition(Vector2f(442, 550));
    FloatRect dimension3 = text1.getGlobalBounds();
    printf("dimension du texte escape %f\n", dimension3.width);

    Event evenement;
    while (fenetre.isOpen())
    {
        fenetre.clear(Color::Black);
        fenetre.draw(gameover);
        fenetre.draw(text);
        fenetre.draw(text1);
        fenetre.display();
        while (fenetre.pollEvent(evenement))
        {
            switch (evenement.type)
            {
            case Event::KeyPressed:
                if (evenement.key.code == Keyboard::Return)
                    printf("Relancer le jeu"); //A modifier pour que le jeu recommence
                if (evenement.key.code == Keyboard::Escape)
                    fenetre.close();

            }
        }
    }
    return 0;
}
